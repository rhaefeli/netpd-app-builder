netpd
=====

is a CRNMME (Collaborative Realtime Networked Music Making Environment)
written in Pure Data. It allows many users to have a realtime jam
sessions with each other, connected over the net.
Users might contribute their own netpd-ized patches a.k.a. instruments
or use pre-existing ones. The set of patches, as well as the state of
each is synchronized between peers. Although audio is rendered locally,
all peers share the same experience.

  Read more:
  https://netpd.org

This is an all-in-one ready-to-go macOS application bundle containing:
  - Pure Data
  - netpd
  - netpd-instruments
  - externals:
      * iemlib
      * iemnet
      * osc
      * slip


intro
=====

* Launch the netpd.app. Chat automatically connects to the server. You
  can now talk to other users currently online.
  Click *list* to get a list all connnected users.

* Click the unpatch button in chat to launch the unpatch instrument manager.
  If there is already a session going on, the instruments used in the running
  session are automatically loaded (they are first downloaded from other users,
  if necessary).

* Load instruments into the current session by clicking on any of the
  instrument names in the upper scroll list.
  Alternatively, just type the name of the instrument (without the
  extension `.pd`) into the input box and hit enter.

* Show an instrument's GUI by clicking on its name in unpatch's lower section.
  You can now manipulate the instrument's parameters. Any changes are instan-
  taneously synchronized between clients. Anyone can manipulate anything, so
  please be cautious and considerate when joining an ongoing session.

If you're lucky, someone is online and might help you get started. This is the
easiest and fastest way to get accustomed to all the instruments and how they
interact with each other. Of course, you can explore the instruments on your
own as well. Maybe start with "untik" and "sine".


help
====

If you need help, have questions about netpd, want to get into
contact with other netpd enthusiasts, please visit:

  https://netpd.org/community


copyright
=========

2008-2024, Roman Haefeli <roman@netpd.org>
Published under the GNU Public License (GPL-2)


links
=====

netpd:
    https://netpd.org
    https://untalk.netpd.org
    https://discord.gg/3FYRbTMhpG
    https://github.com/reduzent/netpd
    https://github.com/reduzent/netpd-instruments

Pure Data:
    https://puredata.info
    http://msp.ucsd.edu/software.html
