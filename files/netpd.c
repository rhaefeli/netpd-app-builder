#include <string.h>
#include <windows.h>
#include <libgen.h>

int main(int argc, char *argv[])
{
    char exec_path[1024];
    GetModuleFileName(NULL, exec_path, 1024);
    strcpy(exec_path, "\"");
    strcat(exec_path, dirname(exec_path));
    strcat(exec_path, "\\pd-netpd\\bin\\pd.exe\" -open pd-netpd\\netpd\\main.pd");
    WinExec(exec_path, SW_HIDE);
    return 0;
}
