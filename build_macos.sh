#!/bin/bash

# Defaults
APP_NAME='netpd'

BUNDLE_VERSION=$(date +%Y-%m-%d)

EXTERNAL_URLS=(
  'https://puredata.info/downloads/kiosk-plugin/releases/1.0/kiosk-plugin.zip'
  'https://netpd.org/~roman/deken/iemlib%5bv1.22%5d(Darwin-amd64-32)(Darwin-arm64-32).dek'
  'https://netpd.org/~roman/deken/iemnet%5bv0.3.0%5d(Darwin-amd64-32)(Darwin-arm64-32).dek'
  'https://netpd.org/~roman/deken/osc%5bv0.2%5d(Darwin-amd64-32)(Darwin-arm64-32).dek'
  'https://netpd.org/~roman/deken/slip%5bv0.1%5d(Darwin-amd64-32)(Darwin-arm64-32).dek'
)

WISH_APP_URL="https://netpd.org/~roman/tmp/wish-shell-universal-8.6.15.tar.gz"

#SIGNING_ID="-"
SIGNING_ID="Developer ID Application: Zurcher Hochschule der Kunste (D95XR8PG48)"
ENTITLEMENTS_FILE="pure-data/mac/stuff/pd.entitlements"

BINARY_FILES=(
  'Contents/Resources/bin/pd'
  'Contents/Resources/bin/pd-watchdog'
  'Contents/Resources/bin/pdreceive'
  'Contents/Resources/bin/pdsend'
  'Contents/Resources/extra/bob~/bob~.d_fat'
  'Contents/Resources/extra/bonk~/bonk~.d_fat'
  'Contents/Resources/extra/choice/choice.d_fat'
  'Contents/Resources/extra/fiddle~/fiddle~.d_fat'
  'Contents/Resources/extra/iemlib/iemlib.d_amd64'
  'Contents/Resources/extra/iemlib/iemlib.d_arm64'
  'Contents/Resources/extra/iemnet/libiemnet.d_amd64.dylib'
  'Contents/Resources/extra/iemnet/libiemnet.d_arm64.dylib'
  'Contents/Resources/extra/iemnet/tcpclient.d_amd64'
  'Contents/Resources/extra/iemnet/tcpclient.d_arm64'
  'Contents/Resources/extra/iemnet/tcpreceive.d_amd64'
  'Contents/Resources/extra/iemnet/tcpreceive.d_arm64'
  'Contents/Resources/extra/iemnet/tcpsend.d_amd64'
  'Contents/Resources/extra/iemnet/tcpsend.d_arm64'
  'Contents/Resources/extra/iemnet/tcpserver.d_amd64'
  'Contents/Resources/extra/iemnet/tcpserver.d_arm64'
  'Contents/Resources/extra/iemnet/udpclient.d_amd64'
  'Contents/Resources/extra/iemnet/udpclient.d_arm64'
  'Contents/Resources/extra/iemnet/udpreceive.d_amd64'
  'Contents/Resources/extra/iemnet/udpreceive.d_arm64'
  'Contents/Resources/extra/iemnet/udpsend.d_amd64'
  'Contents/Resources/extra/iemnet/udpsend.d_arm64'
  'Contents/Resources/extra/iemnet/udpserver.d_amd64'
  'Contents/Resources/extra/iemnet/udpserver.d_arm64'
  'Contents/Resources/extra/loop~/loop~.d_fat'
  'Contents/Resources/extra/lrshift~/lrshift~.d_fat'
  'Contents/Resources/extra/osc/packOSC.d_amd64'
  'Contents/Resources/extra/osc/packOSC.d_arm64'
  'Contents/Resources/extra/osc/pipelist.d_amd64'
  'Contents/Resources/extra/osc/pipelist.d_arm64'
  'Contents/Resources/extra/osc/routeOSC.d_amd64'
  'Contents/Resources/extra/osc/routeOSC.d_arm64'
  'Contents/Resources/extra/osc/unpackOSC.d_amd64'
  'Contents/Resources/extra/osc/unpackOSC.d_arm64'
  'Contents/Resources/extra/pd~/pdsched.d_fat'
  'Contents/Resources/extra/pd~/pd~.d_fat'
  'Contents/Resources/extra/pique/pique.d_fat'
  'Contents/Resources/extra/sigmund~/sigmund~.d_fat'
  'Contents/Resources/extra/slip/slipdec.d_amd64'
  'Contents/Resources/extra/slip/slipdec.d_arm64'
  'Contents/Resources/extra/slip/slipenc.d_amd64'
  'Contents/Resources/extra/slip/slipenc.d_arm64'
  'Contents/Resources/extra/stdout/stdout.d_fat'
)

# parse args
usage="$(basename "$0") [-h] [-k] -- build a netpd app bundle for macOS
where:
    -h|--help         show this help text
    -k|--skip-upload  do not upload resulting app bundle
    -l|--skip-image   do not create a disk image of bundle
                      (this implies --skip-upload)
"

SKIP_UPLOAD=false
SKIP_IMAGE=false
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      echo "$usage"
      exit 0
      shift
      ;;
    -k|--skip-upload)
      SKIP_UPLOAD=true
      shift
      ;;
    -l|--skip-image)
      SKIP_IMAGE=true
      shift
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

# fail helper function
function fail {
  echo "abort due some error"
  exit 1
}

# change to working directory
cd $(dirname $0)

echo "=== Run some checks ==="
echo -n "Are we on a Mac at all? "
if [ "$(uname -s)" != "Darwin" ]
then
  echo "no"
  echo "This is not a Mac. No point in building an"
  echo "app bundle here. Exiting."
  exit 1
else
  echo "yes"
fi

echo -n "Is XCode installed? "
if ! [ -d "/Library/Developer/CommandLineTools/"  ]
then
  echo "no"
  echo "Xcode Command Line Tools are not found on this system."
  echo "Install them by typing the following into your terminal:"
  echo "xcode-select --install"
  exit 1
else
  echo "yes"
fi

echo -n "Is autoconf, automake, libtool installed? "
missing=0
for tool in autoconf automake libtool
do
  if ! which $tool > /dev/null
  then
    missing=$(( missing + 1 ))
  fi
done
if [ "$missing" != 0 ]
then
  echo "no"
  echo "autotools are not installed."
  echo "you can install them with homebrew with the command:"
  echo "brew install autoconf automake libtool"
  if ! which brew > /dev/null
  then
    echo "You can install brew with the command:"
    echo '/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"'
  fi
  exit 1
else
  echo "yes"
fi

echo "=== Check out submodules (pure-data, netpd) ==="
# manage git submodules
# bring them all the configured commit
git submodule update --init --recursive || fail
# purge any local changes
git submodule foreach --recursive git reset --hard || fail

echo "=== Detect Version of ${APP_NAME} ==="
APP_VERSION=$(sed -n 's|^\#X text [0-9][0-9]* [0-9][0-9]* VERSION \(.*\);|\1|p' netpd/includes/netpd-meta.pd | sed 's/ /./g')
echo "Detected version: ${APP_VERSION}"

echo "=== Define LOADER_PLUGIN_CONTENT ==="
LOADER_PLUGIN_CONTENT="
set PD_MAJOR_VERSION $(cut -d'.' -f1 <<< $APP_VERSION)
set PD_MINOR_VERSION $(cut -d'.' -f2 <<< $APP_VERSION)
set PD_BUGFIX_VERSION $(cut -d'.' -f3 <<< $APP_VERSION)
set PD_TEST_VERSION \"\"

after idle  [list open_file [file normalize [file join $::current_plugin_loadpath .. netpd main.pd]]]
"  || fail

echo "=== Build Pure Data ==="
# build Pure Data
(
  cd pure-data
  patch tcl/pd_docsdir.tcl ../files/pd_docsdir.tcl.patch || fail
  patch tcl/pdwindow.tcl ../files/pdwindow.tcl.patch || fail
  patch tcl/pdtk_canvas.tcl ../files/pdtk_canvas.tcl.patch || fail
  patch src/s_audio_jack.c ../files/s_audio_jack.c.patch  || fail
  patch src/s_file.c ../files/s_file.c.patch || fail
  curl -o mac/stuff/wish-shell.tgz "${WISH_APP_URL}" || fail
  ./autogen.sh || fail
  ./configure \
    CFLAGS="-arch x86_64 -arch arm64" \
    --enable-jack \
    --disable-jack-framework \
    --disable-locales || fail
  make -j || fail
  make app || fail
) || fail

echo "=== Rename Pd.app to ${APP_NAME}.app ==="
# rename app
rm -rf ${APP_NAME}.app
mv $(find pure-data -iname Pd-*.app) ${APP_NAME}.app || fail

echo "=== Add patches ==="
cp -R netpd ${APP_NAME}.app/Contents/Resources/ || fail

echo "=== Install externals ==="
# install external libraries
(
  cd ${APP_NAME}.app/Contents/Resources/extra
  for url in ${EXTERNAL_URLS[*]}
  do
    package=$(basename "$url" | sed -e 's/-.*$//;s/%.*$//')
    echo "Installing $package"
    curl -s -O "$url" || fail
    archive="$(basename "$url")"
    unzip -q "$archive" || fail
    rm "$archive"
  done
) || fail

echo "=== Customize ${APP_NAME}.app ==="
# customize app
cp files/README.txt.macos ${APP_NAME}.app/Contents/Resources/tcl/about.txt || fail
cp files/netpd.icns ${APP_NAME}.app/Contents/Resources/pd.icns || fail
sed -i.bak "1 s|\$| $APP_VERSION|;2 s|\$|======|" \
  ${APP_NAME}.app/Contents/Resources/tcl/about.txt || fail
rm ${APP_NAME}.app/Contents/Resources/tcl/about.txt.bak || fail # we need the extra loop because of BSD sed
cp files/kiosk.cfg ${APP_NAME}.app/Contents/Resources/extra/kiosk-plugin/ || fail
echo "$LOADER_PLUGIN_CONTENT" >> "${APP_NAME}.app/Contents/Resources/extra/loader-plugin.tcl" || fail

echo "=== Set Version in Info.plist ==="
/usr/libexec/PlistBuddy -c "Set:CFBundleVersion \"${APP_VERSION}\"" ${APP_NAME}.app/Contents/Info.plist || fail
/usr/libexec/PlistBuddy -c "Set:CFBundleShortVersionString \"${APP_VERSION}\"" ${APP_NAME}.app/Contents/Info.plist || fail

echo "=== Codesign every binary in the app ==="
# "code signing" which also sets entitlements
# note: "-" identity results in "ad-hoc signing" aka no signing is performed
# for one, this allows loading un-validated external libraries on macOS 10.15+:
# https://cutecoder.org/programming/shared-framework-hardened-runtime

# Sign included binaries
for FILE in "${BINARY_FILES[@]}"
do
    echo "Codesigning binary file: ${FILE}"
    codesign \
      --force \
      --sign "${SIGNING_ID}" \
      --options runtime \
      --timestamp \
      --entitlements "${ENTITLEMENTS_FILE}" \
      "${APP_NAME}.app/${FILE}" || fail
done

echo "=== Now recursively sign the whole app ==="
codesign \
  --deep \
  --force \
  --sign "${SIGNING_ID}" \
  --options runtime \
  --timestamp \
  --entitlements "${ENTITLEMENTS_FILE}" \
  "${APP_NAME}.app" || fail

echo "=== Create a compressed image (.dmg) of the ${APP_NAME}.app ==="
if $SKIP_IMAGE; then
  echo "skipping"
  SKIP_UPLOAD=true
else
  # make a dmg image from app bundle
  dmgname="${APP_NAME}-${APP_VERSION}-macos-${BUNDLE_VERSION}.dmg"
  dmgshortname="${APP_NAME}-${APP_VERSION}-macos.dmg"
  dmgdir=dmgdir
  mkdir $dmgdir || fail
  cp files/README.txt.macos ${dmgdir}/README.txt || fail
  cp netpd/LICENSE.txt ${dmgdir}/LICENSE.txt || fail
  mv ${APP_NAME}.app $dmgdir || fail
  hdiutil create -volname ${APP_NAME} -srcfolder $dmgdir -ov -format UDZO ${APP_NAME}-${APP_VERSION}-macos-${BUNDLE_VERSION}.dmg || fail
fi

echo "=== Notarize the app bundle at apple notarization service ==="
xcrun notarytool submit \
  "${APP_NAME}-${APP_VERSION}-macos-${BUNDLE_VERSION}.dmg" \
  --keychain-profile "notarytool-password" \
  --wait || fail

echo "=== Upload dmg file to netpd.org ==="
echo -n "Upload new bundle to netpd.org... "
if  $SKIP_UPLOAD; then
  # clean up workdir only
  echo "skipping"
else
  scp -q "${dmgname}" "all@netpd.org:public_html/software/" || fail
  rm -r "${dmgname}" || fail
  ssh all@netpd.org ln -f -s "${dmgname}" "public_html/software/${dmgshortname}" || fail
  ssh all@netpd.org sed -i "2s/[0-9]\.[0-9]\.[0-9]/${APP_VERSION}/" public_html/software/.htaccess || fail
  echo "done"
fi


echo "=== Clean up ==="
rm -rf $dmgdir
git submodule foreach --recursive git reset --hard || fail

echo "=== Done. Exiting now ==="
exit 0
