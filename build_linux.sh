#!/bin/bash


# Where do I get pre-compiled packages of tcl/tk ?
#
# https://gitlab.com/teclabat/tcltk/-/packages
#
# Nightly builds. Artefacts are not kept forever.

# Defaults
APP_NAME='netpd'
APP_DIR="${APP_NAME}.AppDir"
ICON="${APP_NAME}.png"

BUNDLE_VERSION=$(date +%Y%m%d)

TCLTK_VERSION="8.6.15"
TCLTK_URL="https://netpd.org/~roman/tmp/tcltk-${TCLTK_VERSION}-$(uname -m).tar.gz"

ARCH=$(uname -m)
case $ARCH in
  x86_64)
    ARCH=amd64
    ;;
  i486 | i586 | i686)
    ARCH=i386
    ;;
  armv6 | arm6l | arm7 | arm7l | armv7l)
    ARCH=armv7
    ;;
  aarch64)
    ARCH=arm64
    ;;
esac

EXTERNAL_URLS=(
  'https://puredata.info/downloads/kiosk-plugin/releases/1.0/kiosk-plugin.zip'
  "https://netpd.org/~roman/deken/iemlib%5bv1.22%5d(Linux-${ARCH}-32).dek"
  "https://netpd.org/~roman/deken/iemnet%5bv0.3.0%5d(Linux-${ARCH}-32).dek"
  "https://netpd.org/~roman/deken/osc%5bv0.3%5d(Linux-${ARCH}-32).dek"
  "https://netpd.org/~roman/deken/slip%5bv0.1%5d(Linux-${ARCH}-32).dek"
)

CWD="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# parse args
usage="$(basename "$0") -- build a netpd AppImage for Linux
where:
    -h|--help         show this help text
    -k|--skip-upload  do not upload resulting AppImage
    -l|--skip-image   do not create a AppImage from AppDir folder
"

SKIP_UPLOAD=false
SKIP_IMAGE=false
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      echo "$usage"
      exit 0
      shift
      ;;
    -k|--skip-upload)
      SKIP_UPLOAD=true
      shift
      ;;
    -l|--skip-image)
      SKIP_IMAGE=true
      shift
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

# fail helper function
function fail {
  echo "abort due some error"
  exit 1
}

echo "=== Detect arch for Deken packages ==="
echo "Detected architecture: $ARCH"

# change to working directory
cd "${CWD}"

echo "=== Run some checks ==="
echo -n "Are we on a Linux at all? "
if [ "$(uname -s)" != "Linux" ]
then
  echo "no"
  echo "This is not a Linux System. No point in building an"
  echo "AppImage here. Exiting."
  exit 1
else
  echo "yes"
fi

echo -n "Are the necessary tools installed? "
missing=0
for tool in autoconf automake gcc patchelf curl unzip appimagetool desktop-file-validate appstreamcli
do
  if ! which $tool > /dev/null
  then
    echo "no"
    echo "'$tool' is not found. Please install it."
    exit 1
  fi
done
if [ "$missing" == 0 ]
then
  echo "yes"
fi

echo "=== Check out submodules (pure-data, netpd) ==="
# manage git submodules
# bring them all the configured commit
git submodule update --init --recursive || fail
# purge any local changes
git submodule foreach --recursive git reset --hard || fail

echo "=== Detect Version of ${APP_NAME} ==="
APP_VERSION=$(sed -n 's|^\#X text [0-9][0-9]* [0-9][0-9]* VERSION \(.*\);|\1|p' netpd/includes/netpd-meta.pd | sed 's/ /./g')
echo "Detected version: ${APP_VERSION}"

echo "=== Define LOADER_PLUGIN_CONTENT ==="
LOADER_PLUGIN_CONTENT="
set PD_MAJOR_VERSION $(cut -d'.' -f1 <<< $APP_VERSION)
set PD_MINOR_VERSION $(cut -d'.' -f2 <<< $APP_VERSION)
set PD_BUGFIX_VERSION $(cut -d'.' -f3 <<< $APP_VERSION)
set PD_TEST_VERSION \"\"

after idle  [list open_file [file normalize [file join $::current_plugin_loadpath .. .. .. share netpd main.pd]]]
"  || fail

echo "=== Create AppDir ==="
mkdir -p "${CWD}/${APP_DIR}" || fail

echo "=== Install tcl/tk to AppDir ==="
curl -o tcltk86.tar.gz "${TCLTK_URL}" || fail
tar xf tcltk86.tar.gz -C "${CWD}/${APP_DIR}" || fail
patchelf --set-rpath \$ORIGIN/../lib "${CWD}/${APP_DIR}/bin/wish8.6" "${CWD}/${APP_DIR}/bin/tclsh8.6" || fail
mv "${CWD}/${APP_DIR}/bin/wish8.6" "${CWD}/${APP_DIR}/bin/wish" || fail
mv "${CWD}/${APP_DIR}/bin/tclsh8.6" "${CWD}/${APP_DIR}/bin/tclsh" || fail
rm tcltk86.tar.gz || fail

echo "=== Build and install Pure Data ==="
(
  cd pure-data
  patch tcl/pd_docsdir.tcl ../files/pd_docsdir.tcl.patch || fail
  patch tcl/pdwindow.tcl ../files/pdwindow.tcl.patch || fail
  patch tcl/pdtk_canvas.tcl ../files/pdtk_canvas.tcl.patch || fail
  patch src/s_audio_jack.c ../files/s_audio_jack.c.patch  || fail
  patch src/s_file.c ../files/s_file.c.patch || fail
  ./autogen.sh || fail
  ./configure \
    --enable-jack \
    --disable-locales \
    --prefix "${CWD}/${APP_DIR}" || fail
  make -j || fail
  make install || fail
) || fail

echo "=== Fix path in pd-gui ==="
sed -i 's|^prefix=.*|prefix="${DIR}"|' "${CWD}/${APP_DIR}/bin/pd-gui" || fail

echo "=== Install netpd ==="
cp -r netpd "${CWD}/${APP_DIR}/share/"

echo "=== Install all libraries used by pd and wish ==="
exclude_paths="*/libc.so.*:*/libarmmem.*.so.*:*/libdl.so.*:*/libglib-.*.so.*:*/libgomp.so.*:*/libgthread.*.so.*:*/libm.so.*:*/libpthread.*.so.*:*/libpthread.so.*:*/libstdc++.so.*:*/libgcc_s.so.*:*/libpcre.so.*:*/libz.so.*:*/libjack.so.*:*/libpipewire-*:*/libtcl8.6.so:*/libtk8.6.so"

check_in_path() {
  local needle=$1
  local p
  local patterns
  shift
  patterns="$@"
  while [ "${patterns}" ]; do
    p=${patterns%%:*}
    [ "$patterns" = "$p" ] && patterns='' || patterns="${patterns#*:}"

    case "${needle}" in
      ${p})
        echo "${needle}"
        break
        ;;
    esac
  done | grep . >/dev/null
}

LIBRARIES="$( ( \
  ldd "${CWD}/${APP_DIR}/bin/pd" && \
  ldd "${CWD}/${APP_DIR}/bin/wish"  ) | \
  sort | \
  grep "=>" | \
  cut -d'=' -f2  | \
  cut -d' ' -f2 | \
  uniq)" || fail

libdir="${CWD}/${APP_DIR}/lib"
mkdir -p "${libdir}" || fail
for lib in ${LIBRARIES}
do
  if ! check_in_path "${lib}" "${exclude_paths}"
  then
    install -c "${lib}" "${libdir}" || fail
    patchelf --set-rpath \$ORIGIN "${libdir}/$(basename ${lib})" || fail
  fi
done || fail

echo "=== patchelf pd and wish ==="
patchelf --set-rpath \$ORIGIN/../lib "${CWD}/${APP_DIR}/bin/pd" || fail
patchelf --set-rpath \$ORIGIN/../lib "${CWD}/${APP_DIR}/bin/wish" || fail

echo "=== Install externals ==="
(
  cd "${CWD}/${APP_DIR}/lib/pd/extra/"
  for url in ${EXTERNAL_URLS[*]}
  do
    package=$(basename "$url" | sed -e 's/-.*$//;s/%.*$//')
    echo "Installing $package"
    curl -s --output "${package}.zip" "$url" || fail
    unzip -q -o "${package}.zip" || fail
    rm "${package}.zip"
  done
) || fail

echo "=== Apply some customizations ==="
cp files/kiosk.cfg "${CWD}/${APP_DIR}/lib/pd/extra/kiosk-plugin" || fail
echo "$LOADER_PLUGIN_CONTENT" >> "${CWD}/${APP_DIR}/lib/pd/extra/loader-plugin.tcl" || fail
cp files/README.txt.linux "${CWD}/${APP_DIR}/lib/pd/tcl/about.txt" || fail
sed -i "1 s|\$| $APP_VERSION|;2 s|\$|======|" \
  "${CWD}/${APP_DIR}/lib/pd/tcl/about.txt" || fail

echo "=== Install AppImage files ==="
install -c files/AppRun "${CWD}/${APP_DIR}/" || fail
install -c --mode=0644 files/org.netpd.netpd.desktop "${CWD}/${APP_DIR}/" || fail
install -c --mode=0644 files/org.netpd.netpd.desktop "${CWD}/${APP_DIR}/share/applications/" || fail
install -c --mode=0644 files/org.netpd.netpd.metainfo.xml "${CWD}/${APP_DIR}/share/metainfo/" || fail
install -c --mode=0644 "files/${ICON}" "${CWD}/${APP_DIR}/" || fail

if $SKIP_IMAGE
then
   SKIP_UPLOAD=true
else
  echo "=== Create AppImage from AppDir ==="
  appimagetool "${APP_DIR}" || fail
  IMG_ARCH="$(ls *.AppImage | sed -n 's/netpd-\(.*\)\.AppImage/\1/p')"  || fail
  IMG_FILE="${APP_NAME}-${APP_VERSION}-${BUNDLE_VERSION}-${IMG_ARCH}.AppImage" || fail
  mv "${APP_NAME}-${IMG_ARCH}.AppImage" "${IMG_FILE}" || fail

  echo "=== Clean up AppDir ==="
  rm -rf "${APP_DIR}" || fail
fi

echo "=== Reset git repositories ==="
git submodule foreach --recursive git reset --hard || fail

if ! $SKIP_UPLOAD
then
  echo "=== Upload AppImage bundle ==="
  scp -q "${IMG_FILE}" all@netpd.org:public_html/software/

  echo "=== Clean up local AppImage bundle ===="
  rm "${IMG_FILE}" || fail

  echo "=== Create shortcut in dowload directory ==="
  ssh all@netpd.org ln -f -s "${IMG_FILE}" "public_html/software/${APP_NAME}-${APP_VERSION}-${IMG_ARCH}.AppImage" || fail

  echo "=== Update redirect target in .htaccess ==="
  ssh all@netpd.org grep -n "${IMG_ARCH}.AppImage" public_html/software/.htaccess > /dev/null || fail
  LINE_NUMBER="$(ssh all@netpd.org grep -n "${IMG_ARCH}.AppImage" public_html/software/.htaccess | cut -d":" -f1)"
  ssh all@netpd.org sed -i "${LINE_NUMBER}s/[0-9]\.[0-9]\.[0-9]/${APP_VERSION}/" public_html/software/.htaccess || fail
fi

echo "=== Done. Exiting now ==="
exit 0
