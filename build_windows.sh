#!/bin/bash

APP_NAME="netpd"
BUNDLE_VERSION=$(date +%Y-%m-%d)
TCLTK_VERSION="8.6.16"
TCLTK_URL="https://netpd.org/~roman/tmp/tcltk-${TCLTK_VERSION}-windows-x86_64.tar.gz"

usage="$(basename "$0") [-h] [-k] -- build a netpd bundle for Windows

where:
    -h|--help         show this help text
    -k|--skip-upload  do not upload resulting app bundle"

SKIP_UPLOAD=false
POSITIONAL_ARGS=()

while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      echo "$usage"
      exit 0
      shift
      ;;
    -k|--skip-upload)
      SKIP_UPLOAD=true
      shift
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

CWD="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"


# Libraries
EXTERNAL_URLS=(
  'https://puredata.info/Members/musil/software/iemlib/1.22/iemlib%5Bv1.22%5D(Darwin-amd64-32)(Darwin-i386-32)(Linux-amd64-32)(Windows-amd64-32)(Windows-i386-32)(Sources).dek'
  'https://puredata.info/Members/zmoelnigbot/software/iemnet/0.3.0/iemnet%5Bv0.3.0%5D(Darwin-amd64-32)(Linux-amd64-32)(Linux-arm64-32)(Linux-armv7-32)(Linux-i386-32)(Windows-amd64-32)(Windows-i386-32).dek'
  'https://puredata.info/Members/rdz/software/osc/0.2~git20181006/osc%5Bv0.2~git20181006%5D(Windows-amd64-32).dek'
  'https://puredata.info/Members/rdz/software/slip/0.1~git20181006/slip%5Bv0.1~git20181006%5D(Windows-amd64-32).dek'
  'https://puredata.info/downloads/kiosk-plugin/releases/1.0/kiosk-plugin.zip'
)

function fail {
  echo "abort due some error"
  exit 1
}

echo -n "Are the necessary tools installed? "
missing=0
for tool in git autoconf automake gcc curl unzip tar patch sed unix2dos
do
  if ! which $tool > /dev/null
  then
    echo "no"
    echo "'$tool' is not found. Please install it."
    exit 1
  fi
done
if [ "$missing" == 0 ]
then
  echo "yes"
fi

echo "=== Check out submodules (pure-data, netpd) ==="
# manage git submodules
# bring them all the configured commit
git submodule update --init --recursive || fail
# purge any local changes
git submodule foreach --recursive git reset --hard || fail

APP_VERSION=$(sed -n 's|^\#X text [0-9][0-9]* [0-9][0-9]* VERSION \(.*\);|\1|p' "$(dirname $0)/${APP_DIR}/netpd/includes/netpd-meta.pd") || fail

APP_DIR="${APP_NAME}-${APP_VERSION}"

LOADER_PLUGIN_CONTENT="
set PD_MAJOR_VERSION $(cut -d'.' -f1 <<< $APP_VERSION)
set PD_MINOR_VERSION $(cut -d'.' -f2 <<< $APP_VERSION)
set PD_BUGFIX_VERSION $(cut -d'.' -f3 <<< $APP_VERSION)
set PD_TEST_VERSION \"\"

after idle  [list open_file [file normalize [file join $::current_plugin_loadpath .. netpd main.pd]]]
"  || fail

cd "${CWD}"

echo "=== Compile Pd (Windows target) ==="
(
  cd pure-data
  echo "=== Replace icon ==="
  cp ../files/netpd_icon.ico tcl/pd.ico || fail
  echo "=== Apply patches to Pd sources ==="
  patch tcl/pd_docsdir.tcl ../files/pd_docsdir.tcl.patch || fail
  patch tcl/pdwindow.tcl ../files/pdwindow.tcl.patch || fail
  patch tcl/pdtk_canvas.tcl ../files/pdtk_canvas.tcl.patch || fail
  patch src/s_audio_jack.c ../files/s_audio_jack.c.patch  || fail
  patch src/s_file.c ../files/s_file.c.patch || fail
  echo "=== Run autogen.sh ==="
  ./autogen.sh || fail
  echo "=== Run configure ==="
  ./configure \
    --with-wish=wish86.exe \
    --disable-locales 
  echo "=== Run make ==="
  make -j || fail

  cd msw || fail
  echo "===  Download TclTk Version ${TCLTK_VERSION} ==="
  curl "${TCLTK_URL}" | tar xf - || fail
  echo "=== Build a Pd bundle for windows ==="
  ./msw-app.sh --tk "tcltk-${TCLTK_VERSION}" netpd || fail
  echo "=== Clear tcltk dir ===="
  rm -rf "tcltk-${TCLTK_VERSION}"
  echo "=== replace pdfontloader for wish86 ==="
  cd pd-netpd/bin || fail
  rm pdfontloader.dll
  unzip ../../pdfontloader-64bit-for-wish86.zip
) || fail

echo "=== Create Bundle dir ==="
mkdir "${APP_DIR}"
mv pure-data/msw/pd-netpd "${APP_DIR}/netpd"
echo "=== Rename pd.exe -> netpd.exe ==="
mv "${APP_DIR}/netpd/bin/pd.exe" "${APP_DIR}/netpd/bin/netpd.exe"
echo "=== Install externals and kiosk-plugin ==="  
(
  cd "${APP_DIR}/netpd/extra"
  for url in ${EXTERNAL_URLS[*]}
  do
    package=$(basename "$url" | sed -e 's/-.*$//;s/%.*$//')
    echo "=== Installing $package ==="
    curl -s -O "$url" || fail
    archive="$(basename "$url")"
    unzip -q "$archive" || fail
    rm "$archive"
  done
) || fail

echo "=== Install netpd ==="
cp -r netpd "${APP_DIR}/netpd/." || fail
echo "=== Remove any vcs related files ==="
(
  cd "${APP_DIR}/netpd/netpd"
  rm -rf .git .gitignore .gitmodules instruments/.git
) || fail

echo "=== Configure kiosk-plugin ==="
cp files/kiosk.cfg "${APP_DIR}/netpd/extra/kiosk-plugin/" || fail
echo "=== Write loader-plugin ===" 
echo "$LOADER_PLUGIN_CONTENT" > "${APP_DIR}/netpd/extra/loader-plugin.tcl" 
echo "=== Copy link file ==="
cp files/netpd.lnk "${APP_DIR}" || fail
echo "=== Install README ==="
cp files/README.txt.win "${APP_DIR}/README.txt" || fail
sed -i "1 s|\$| ${APP_VERSION}|;2 s|\$|======|" \
  ${APP_DIR}/README.txt || fail
unix2dos ${APP_DIR}/README.txt || fail
echo "=== Create about.txt for netpd ==="
cp ${APP_DIR}/README.txt ${APP_DIR}/netpd/tcl/about.txt || fail

echo "=== Create archive: '${APP_NAME}-${APP_VERSION}-windows-${BUNDLE_VERSION}.zip'"
zip -q -r "${APP_NAME}-${APP_VERSION}-windows-${BUNDLE_VERSION}.zip" "${APP_NAME}-${APP_VERSION}"

if $SKIP_UPLOAD; then
  echo "=== Clean up workdir ==="
  rm -rf "${APP_DIR}" || fail
else
  echo "=== Upload bundle ==="
  scp -q "${APP_NAME}-${APP_VERSION}-windows-${BUNDLE_VERSION}.zip" all@netpd.org:public_html/software/ || fail
  echo "=== Clean up workdir and bundle ==="
  rm -rf "${APP_DIR}" "${APP_NAME}-${APP_VERSION}-windows-${BUNDLE_VERSION}.zip"
  echo "=== Create shortcut in dowload directory ==="
  ssh all@netpd.org ln -f -s "${APP_NAME}-${APP_VERSION}-windows-${BUNDLE_VERSION}.zip" "public_html/software/${APP_NAME}-${APP_VERSION}-windows.zip" || fail
  echo "=== Update redirect target in .htaccess ==="
  ssh all@netpd.org sed -i "1s/[0-9]\.[0-9]\.[0-9]/${APP_VERSION}/" public_html/software/.htaccess || fail
fi
