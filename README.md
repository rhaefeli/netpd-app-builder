netpd-app-builder
-----------------

This repo contains scripts for creating standalone apps from netpd for Windows
and macOS.

What does it do?
----------------

### macOS
For macOS, a netpd.app is created by building Pd from submodule (after applying
some patches). The resutling Pd.app is then modified to include the necessary externals
and patches. The Info.plist is modified to reflect the new name of the app and
the icon is replaced with a custom icon. A plugin is included to set the app's
versiond and to load the custom patch (`netpd/main.pd`).

**NOTE**: Script runs only on **macOS** system.

#### build app only (do not create image)

```
./build_macos.sh --skip-image
```

#### build distributable dmg image containing app

```
./build_macos.sh --skip-upload
```

#### do the whole thing and publish result to netpd.org

```
./build_macos.sh
```

### Windows
For Windows, netpd and Pd are bundled in a single distributable directory. A compiled
launcher binary (netpd.exe) that loads Pd and opens the custom patch is included.
The exe file contains a custom icon.

**NOTE**: Script is tested on Linux, but runs probably on Windows, too (with Bash installed).

#### build zip archive with launcher, Pd and netpd

```
./build_windows.sh --skip-upload
```

#### do the whole thing and publish result to netpd.org

```
./build_windows.sh
```

Bugs
----

Do not report bugs. This is not meant for public use.

